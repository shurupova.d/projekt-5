## XPath'ы первых двух страниц ("1. Выбор полиса" и "2. Оформление") с сайта [СберСтрахование](https://online.sber.insure/store/propertyins/).

**Кнопки**<br>
Квартира: //mat-button-toggle[@id="mat-button-toggle-1"]<br>
Дом: //mat-button-toggle[@id="mat-button-toggle-2"]<br>
<br>
Сдается в аренду (квартира)<br>
Да - //mat-button-toggle-group[div[text()= 'Сдается в аренду']]//span[text()='Да']/ancestor::button<br>
Нет - //mat-button-toggle-group[div[text()= 'Сдается в аренду']]//span[text()='Нет']/ancestor:::button<br>
<br>
Расположена на первом или последнем этаже (квартира)<br>
Да - //mat-button-toggle-group[div[text()= 'Расположена на первом или последнем этаже']]//span[text()='Да']/ancestor::button<br>
Нет - //mat-button-toggle-group[div[text()= 'Расположена на первом или последнем этаже']]//span[text()='Нет']/ancestor::button<br>
<br>
Установлена охранная сигнализация (квартира)<br>
Да - //mat-button-toggle-group[div[text()= 'Установлена охранная сигнализация']]//span[text()='Да']/ancestor::button<br>
Нет - //mat-button-toggle-group[div[text()= 'Установлена охранная сигнализация']]//span[text()='Нет']/ancestor::button<br>
<br>
Применить - //button[contains(@class,'action-black')]<br>
Оформить - //button[@color="accent"] или //button[contains(@class,'mat-button-disabled')]<br>
<br>
Сдается в аренду (дом)<br>
Да - //mat-button-toggle-group[div[text()= 'Сдается в аренду']]//span[text()='Да']/ancestor::button<br>
Нет - //mat-button-toggle-group[div[text()= 'Сдается в аренду']]//span[text()='Нет']/ancestor:::button<br>
<br>
Установлена охранная сигнализация (дом)<br>
Да - //mat-button-toggle-group[div[text()= 'Установлена охранная сигнализация']]//span[text()='Да']/ancestor::button<br>
Нет - //mat-button-toggle-group[div[text()= 'Установлена охранная сигнализация']]//span[text()='Нет']/ancestor::button<br>
<br>
Материал несущих стен (дом)<br>
Кирпич и монолит - //button[@id="mat-button-toggle-73"]<br>
Дерево - //button[@id="mat-button-toggle-75"]<br>
<br>
Применить - //button[contains(@class,'action-black')]<br>
Оформить - //button[@color="accent"] или //button[contains(@class,'mat-button-disabled')]<br>
<br>
Кнопка отрытия календаря при выборе даты страхования - //button[@aria-haspopup="dialog"]<br>
Мужской - //button[@name="mat-button-toggle-group-57"][aria-pressed="true"]<br>
Женский - //button[@name="mat-button-toggle-group-57"][aria-pressed="false"]<br>
Заполнить по Сбер ID - //button[@color="primary"]<br>
Вернуться - //button[contains(@class,'action-black')]<br>
Оформить - //button[@color="accent"] или //button[contains(@class,'mat-accent')]<br>
<br>
**Поля ввода текста**<br>
Поле ввода страховой суммы - //input[@mask='separator.0']<br>
Промкод - //div[@class="mat-form-field-flex ng-tns-c61-2"]<br>
Фамилия - //div[@class="mat-form-field-flex ng-tns-c61-3"]<br>
Имя - //div[@class="mat-form-field-flex ng-tns-c61-4"]<br>
Отчество - //div[@class="mat-form-field-flex ng-tns-c61-5"]<br>
Серия - //div[@class="mat-form-field-flex ng-tns-c61-7"]<br>
Номер - //div[@class="mat-form-field-flex ng-tns-c61-8"]<br>
Кем выдан - //div[@class="mat-form-field-flex ng-tns-c61-10"]<br>
Код подразделения - //div[@class="mat-form-field-flex ng-tns-c61-11"]<br>
Город или населенный пункт - //div[@class="mat-form-field-flex ng-tns-c61-13"]<br>
Улица - //div[@class="mat-form-field-flex ng-tns-c61-14"]<br>
Дом, литера, корпус, строение - //div[@class="mat-form-field-flex ng-tns-c61-15"]<br>
Квартира - //div[@class="mat-form-field-flex ng-tns-c61-16"]<br>
Телефон - //div[@class="mat-form-field-flex ng-tns-c61-17"]<br>
Электронная почта - //div[@class="mat-form-field-flex ng-tns-c61-18"]<br>
Повтор электронной почты - //div[@class="mat-form-field-flex ng-tns-c61-19"]<br>
<br>
**Чекбоксы**<br>
Отчество отсутствует - //[@id="mat-checkbox-1"] //[@id="mat-checkbox-3"]/label<br>
Улица отсутствует - //[@id="mat-checkbox-2"]<br>
<br>
**Датапикеры**<br>
Дата начала - //div[@class="mat-form-field-flex ng-tns-c61-1"]<br>
Дата рождения - //div[@class="mat-form-field-flex ng-tns-c61-6"]<br>
Дата выдачи - //div[@class="mat-form-field-flex ng-tns-c61-9"]<br>
<br>
**Логотип**<br>
Логотип Сбер страхование - //div[@class="sber-logo"]<br>
<br>
**Слайдер**<br>
Слайдер в блоке выбора суммы - //mat-slider<br>
<br>
**Хедер**<br>
Хедер "Что будет застраховано" - //h4[@class="block-header"][1]<br>
<br>
**Колонки под хедером "Страховая защита включения в программу"**<br>
Левая колонка - //div[text()="Чрезвычайная ситуация"]/ancestor::ul<br>
Правая колонка - //div[text()="Стихийные бедствия"]/ancestor::ul<br>
<br>
**Текстовые блоки**<br>
Мебель, техника и ваши вещи - //div[contains(text(),'Мебель')]<br>
Падение летательных аппаратов -  //div[text()="Падение летательных аппаратов и их частей"]<br>
<br>
**Footer**<br>
Футур - //footer<br>
